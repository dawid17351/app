import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("app.txt");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wpisz tekst:");
        String linia_z_wejscia;
        PrintWriter writer = new PrintWriter(new FileOutputStream(file));
        do{
            linia_z_wejscia = scanner.nextLine();
            writer.println(linia_z_wejscia);
        }while (!linia_z_wejscia.equalsIgnoreCase("quit"));
        writer.close();
        System.out.println("Koniec programu");

    }
}
